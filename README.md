> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 10

1. Layout
        >![Layout](/images/Layout.png "Layout")
Tahap pertama, susun layout yang akan dibuat nanti pada aplikasi. Sesuaikan posisi setiap komponennya agar rapi. 

1. Source Code
        >![SourceCode](/images/SourceCode.png "Source Code")
Seperti yang dicoba ketika latihan, import terlebih dahulu segala kebutuhannya. Setelah itu, deklarasikan variabel-variabel yang akan digunakan pada program seperti TextView, EditText, dan Button. Lalu, susun syntax sehingga program dapat menjalankan perintah perkalian.

1. Hasil
        >![Hasil](/images/Hasil.png "Hasil")
